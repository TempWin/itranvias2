<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>El bus</title>

        <!-- Font Awesome 5.2.0 -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <!-- Bootstrap 4.1 -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

        <!-- jQuery UI 1.12.1 -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- Estilos personalizados -->
        <link href='css/estilos.css' rel='stylesheet' />

        <style>
        html {
            height: 100%;
        }
        body {
            font-family: "R Gerstner", sans-serif;
            position: relative;
            min-height: 100%;
        }

        h1 {
            margin-bottom: 20px;
        }

        .contenido {
            padding-top: 30px;
        }

        .footer {
            position: absolute;
            right: 0;
            bottom: 0;
            left: 0;
            padding: 1rem;
            background-color: #545454;
            text-align: center;
            color: #fff;
        }

        .footer a {
            color: #fff;
        }
        </style>
    </head>

    <body>
        <div class="container">
            <div class="contenido">
                <div class="row">
                    <div class="col-12">
                        <h2>Líneas de buses</h2>
<?php

$fecha = new DateTime();
$epoch_time = $fecha->getTimestamp() * 1000;

// URL con la información de todas las líneas
$url_lineas = "http://itranvias.es/queryitr.php?&dato=0&func=1&_=" . $epoch_time;

$curl = curl_init();
// Líneas de autobuses y sus códigos internos
curl_setopt_array($curl, array(
      CURLOPT_URL => $url_lineas, // URL
      CURLOPT_RETURNTRANSFER => true, // Devuelve la respuesta; si falla, también lo indicará
      CURLOPT_SSL_VERIFYPEER => false // Deshabilitamos la verificación SSL
));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);


if ($err) {
        echo "cURL Error #:" . $err;
} else {
    $json_response = json_decode($response, true);
    /*
    echo "<pre>";
    print_r($json_response["lineas"]);
    echo "</pre>";
    */
    
    // Generamos un array con todas las líneas de buses y sus identificadores
    foreach ($json_response["lineas"] as $clave => $array_lineas) {
        $lineas[$array_lineas["id"]]["nombre"] = $array_lineas["nom_comer"];
        $lineas[$array_lineas["id"]]["origen"] = $array_lineas["orig_linea"];
        $lineas[$array_lineas["id"]]["destino"] = $array_lineas["dest_linea"];
    }
    
}

//print_r($lineas);

foreach ($lineas as $codigo_linea => $datos) {
    echo "
                        <p>" . $datos["nombre"] . "</p>
                        <ul>
                            <li>Inicio: " . $datos["origen"] . "</li>
                            <li>Fin: " . $datos["destino"] . "</li>
                        </ul>";
}

foreach ($lineas as $codigo_linea => $datos) {
    $url_linea = "http://itranvias.es/queryitr.php?&dato=" . $codigo_linea . "&func=2&_=" . $epoch_time;
    $curl = curl_init();
    // Líneas de autobuses y sus códigos internos
    curl_setopt_array($curl, array(
          CURLOPT_URL => $url_linea, // URL
          CURLOPT_RETURNTRANSFER => true, // Devuelve la respuesta; si falla, también lo indicará
          CURLOPT_SSL_VERIFYPEER => false // Deshabilitamos la verificación SSL
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    echo "
                        <p>" . $datos["nombre"] . "</p>";

    if ($err) {
            echo "cURL Error #:" . $err;
    } else {
        $json_response = json_decode($response, true);
        $paradas_ida = $json_response["paradas"][0]["paradas"];
        $paradas_vuelta = $json_response["paradas"][1]["paradas"];
        echo "
                        <ul>";
        foreach ($paradas_ida as $clave => $datos) {
            $linea_paradas[$codigo_linea]["id"] = $datos["id"];
            $linea_paradas[$codigo_linea]["parada"] = $datos["parada"];
            echo "
                            <li>" . $datos["parada"] . "</li>";
        }
        echo "
                        </ul>";
        /*
        echo "<pre>";
        print_r($json_response["paradas"][0]["paradas"]);
        echo "</pre>";
        die();
        */
    }
        // foreach ($json_response["paradas"] as $clave => $array_lineas) {

}

echo "
                <pre>";
print_r($linea_paradas);
echo "
                </pre>";



?>   
                
                    </div>
                </div> <!-- row -->
            </div> <!-- contenido -->
        </div> <!-- container -->
        
        

        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- jQuery UI -->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript">
            // Cada vez que coja el foco el input de la fecha, cargaremos DatePicker
            // Empleamos esta solución ya que se pueden ir añadiendo nuevos input después
            // de haber cargado la página
            $('body').on('focus',".datepicker", function(){
                $(this).datepicker({
                    dateFormat: "dd/mm/yy",
                    firstDay: 1,
                    closeText: 'Cerrar',
                    prevText: '< Ant',
                    nextText: 'Sig >',
                    currentText: 'Hoy',
                    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic']
                });
            });

            function nuevo_registro() {
                var wrapper = $(".registro");

                // HTML que se añadirá cada vez que pulsemos el botón de crear nuevo registro
                var registro = '\
                    <div class="row mb-2">\
                        <div class="col-3">\
                                    <select class="custom-select" name="oficinas[]" required>\
                                        <option selected>Oficina</option>\
                                        <option value="c">B&F CORUÑA</option>\
                                        <option value="c2">B&F CORUÑA2</option>\
                                        <option value="c3">B&F CORUÑA3</option>\
                                        <option value="lu2">B&F LUGO2</option>\
                                        <option value="po">B&F PONTEVEDRA</option>\
                                        <option value="s">B&F SANTIAGO</option>\
                                        <option value="s2">B&F SANTIAGO</option>\
                                        <option value="vi">B&F VIGO</option>\
                                        <option value="vi1">B&F VIGO1</option>\
                                    </select>\
                                </div>\
                                <div class="col-2">\
                                    <input type="text" class="form-control" placeholder="Teléfono" name="moviles[]" required>\
                                </div>\
                                <div class="col-2">\
                                    <input type="text" name="fechas[]" class="form-control datepicker " placeholder="Fecha" required>\
                                </div>\
                                <div class="col-2">\
                                    <select class="custom-select" name="operaciones[]" required>\
                                        <option selected>Operación</option>\
                                        <option value="0">Baja</option>\
                                        <option value="1">Alta</option>\
                                    </select>\
                                </div>\
                                <div class="col-1">\
                                     <button type="button" class="btn btn-default borrar" href="#" title="Eliminar registro"><i class="fas fa-minus-circle"></i></button>\
                                 </div>\
                            </div>\
                        </div>\
                ';

                $(wrapper).append(registro);
                // console.log("INFO: nuevo_registro");
            }

            // Eliminar los registros añadidos
            $(document).on('click', '.borrar', function() {
                $(this).closest(".row").remove();
            });
        </script>


    </body>
</html>