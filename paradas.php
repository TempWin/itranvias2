<?php

$fecha = new DateTime();
$epoch_time = $fecha->getTimestamp() * 1000;

if (!isset($_GET["id"])) {
    echo "No se ha pasado ninguna parada";
} else {
    $numero_parada = $_GET["id"];

/*
// URL con la información de la parada
$peticion_parada = "https://itranvias.com/queryitr_v3.php?&dato=" . $numero_parada . "&func=0&_=" . $epoch_time;
$curl = curl_init();

curl_setopt_array($curl, array(
      CURLOPT_URL => $peticion_parada, // URL
      CURLOPT_RETURNTRANSFER => true, // Devuelve la respuesta; si falla, también lo indicará
      CURLOPT_SSL_VERIFYPEER => false // Deshabilitamos la verificación SSL
));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);

if ($err) {
        echo "cURL Error #:" . $err;
} else {
*/
    /*
    // Versión estática
    $info_parada_json = file_get_contents("parada.json");
    $info_itranvias_json = file_get_contents("itranvias.json");
    */
    $peticion_parada = "https://itranvias.com/queryitr_v3.php?&dato=" . $numero_parada . "&func=0&_=" . $epoch_time;
    $curl = curl_init();

    curl_setopt_array($curl, array(
          CURLOPT_URL => $peticion_parada, // URL
          CURLOPT_RETURNTRANSFER => true, // Devuelve la respuesta; si falla, también lo indicará
          CURLOPT_SSL_VERIFYPEER => false // Deshabilitamos la verificación SSL
    ));

    $info_parada_json = curl_exec($curl);
    $info_itranvias_json = file_get_contents("itranvias.json");
    $err = curl_error($curl);
    curl_close($curl);


    //$json_response = json_decode($response, true);
    $info_parada = json_decode($info_parada_json, true);
    $info_itranvias = json_decode($info_itranvias_json, true);

    $total_lineas = $info_itranvias["iTranvias"]["actualizacion"]["lineas"];
    $info_parada_lineas = $info_parada["buses"]["lineas"];
    
    // Rellenamos un array con todos los códigos de línea y sus nombres
    foreach ($total_lineas as $clave => $datos) {
        $lineas_info[$datos["id"]] = $datos["lin_comer"];
    }

    // Rellenamos un array con las líneas (el código) que pasan por esa parada
    foreach ($info_parada_lineas as $clave => $datos) {
        $lineas_parada[] = $datos["linea"];

        // Guardamos la información de cada bus que va a pasar por esa parada
        foreach ($datos["buses"] as $clave => $bus) {
            $buses_parada[$datos["linea"]]["buses"][] = [
                                                            "bus" => $bus["bus"],
                                                            "tiempo" => $bus["tiempo"]
                    ];
        }
    }

    echo "
            <p>Las líneas que pasan por esta parada son: </p>
            <ul>";
    foreach ($lineas_parada as $linea) {
        echo "
                <li>" . $lineas_info[$linea] . " (próximo en " . $buses_parada[$linea]["buses"][0]["tiempo"] . " minutos)</li>";
    }
    echo "
            </ul>";

    // DEBUG
    /*            
    echo "
            <pre>";
    print_r($buses_parada);
    echo "
            </pre>";
    */
}
?>