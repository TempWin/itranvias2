<?php

$fecha = new DateTime();
$epoch_time = $fecha->getTimestamp() * 1000;

// URL con la información de todas las líneas
$url_paradas = "https://itranvias.com/queryitr_v3.php?&dato=228&func=0&_=" . $epoch_time;

// URL con toda la información estática de la aplicación
$idioma = [
    "Gallego" => "gl",
    "Español" => "es",
    "Inglés" => "en"
];

$url = "https://itranvias.com/queryitr_v3.php?&dato=20160101T000000_" . $idioma["Español"] . "_0_20160101T000000&func=7&_=" . $epoch_time;
echo $url;

$info = file_get_contents("itranvias.json");

$json_dec = json_decode($info, true);

$tarifas = $json_dec["iTranvias"]["actualizacion"]["precios"]["tarifas"];

?>

                        <h2>Tarifas</h2>
                        <ul>
<?php 
    foreach ($tarifas as $clave => $valores) {
        echo "
                            <li>" . $valores["tarifa"] . ": " . $valores["precio"] . "€</li>";
    }
?>
                        </ul>
